FROM node:20-alpine3.17

COPY . app
WORKDIR /app

RUN npm install

CMD ["node", "src/server.js"]
